//
//  JSCamera.h
//  JSCamera
//
//  Created by Jason Stelzel on 7/9/19.
//  Copyright © 2019 Jason Stelzel. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for JSCamera.
FOUNDATION_EXPORT double JSCameraVersionNumber;

//! Project version string for JSCamera.
FOUNDATION_EXPORT const unsigned char JSCameraVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <JSCamera/PublicHeader.h>


