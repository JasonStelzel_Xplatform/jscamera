//
//  JSCameraChooserViewController.swift
//  JSCamera
//
//  Created by Jason Stelzel on 7/25/19.
//  Copyright © 2019 Jason Stelzel. All rights reserved.
//

import UIKit

class JSCameraChooserViewController: UIViewController {

    @IBOutlet var ChoosePhotoCamera: UIButton!
    @IBOutlet var ChooseVideoCamera: UIButton!
    public enum JSCameraType {
        case photo
        case video
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    public func goDestination(destination: UIViewController) {
        let vcList = tabBarController?.viewControllers
        if (self.tabBarController != nil){
            let selectedIndex = self.tabBarController?.selectedIndex
            let newList: [UIViewController] = vcList!.enumerated().map { (arg) -> UIViewController in
                let (index, vc) = arg
                return index == selectedIndex ? destination : vc
            }
            print ("Camera vc list = ", newList)
            self.tabBarController?.setViewControllers(newList, animated: false)
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "ChoosePhotoCamera":
            guard let destination = segue.destination as? CameraViewController
                else { fatalError("unexpected view controller for segue") }
            destination.initialCameraType = .photo
            goDestination(destination: destination)
        case "ChooseVideoCamera":
            guard let destination = segue.destination as? CameraViewController
                else { fatalError("unexpected view controller for segue") }
            destination.initialCameraType = .video
            goDestination(destination: destination)
        default:
            guard let destination = segue.destination as? CameraViewController
                else { fatalError("unexpected view controller for segue") }
            destination.initialCameraType = .photo
            goDestination(destination: destination)
        }
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
